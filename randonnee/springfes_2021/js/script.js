$(function(){
  var pagetop = $('.top-btn');
  pagetop.hide();
  $(window).scroll(function () {
    if ($(this).scrollTop() > 600) {
      pagetop.fadeIn();
    } else {
      pagetop.fadeOut();
    }
  });
  pagetop.click(function () {
    $('body, html').animate({ scrollTop: 0 }, 500);
    return false;
  });

  var navPos = $( 'nav' ).offset().top; // グローバルメニューの位置
  var navHeight = $( 'nav' ).outerHeight(); // グローバルメニューの高さ
  $( window ).scroll(function() {
    if ( $( this ).scrollTop() > navPos ) {
      $( 'nav' ).addClass( 'fixed' );
    } else {
      $( 'nav' ).removeClass( "fixed" );
    }
  });

  $('a[href^="#"]').on('click', function() {
    // スクロールの速度
    var speed = 1000;
    // アンカーの値取得
    var href= $(this).attr("href");
    // 移動先を取得
    var target = $(href == "#" || href == "" ? 'html' : href);
    // 移動先を数値で取得
    var position = target.offset().top - [navHeight];
    // スムーススクロール
    $('body,html').animate({scrollTop:position}, speed, 'swing');
    return false;
  });

  var hover = function() {
    $(window).on('load resize orientationchange', function() {
        if($(window).width() <= 768) {
            $('.nav-li,.sns-logo').removeClass('hover');
        } else {
            $('.nav-li,.sns-logo').addClass('hover');
        }
    });
}

$(function(){
    hover();
});

});
