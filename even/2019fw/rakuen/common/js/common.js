//ページ下部ボタン
$(function() {
	var topBtn = $('#page-top');
	topBtn.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
});

$(document).ready(function() {
	$('[data-fancybox]').fancybox({
		smallBtn : true,
		iframe : {
			preload : true
		}
	});
});

