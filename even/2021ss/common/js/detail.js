$(function() {
var animation = anime({
  targets: '.link-btn',
  translateX: 270,
  loop: false,
  delay: function(el, i) { return i * 200; },
  easing: 'easeInOutSine'
});
document.querySelector('.aaa').onclick = function() {
  animation.play();
  animation.reverse();
}
});

//オープニング用
$(function() {
var phTimeline = anime.timeline({
	duration: 6000,
	easing: 'easeOutExpo'
});
phTimeline
.add({
	targets: '.',
	opacity: 0,
} ,600)
.add({
	targets: '.mv02',
	opacity: 1,
} ,800)
.add({
	targets: '.mv03',
	opacity: 1,
} ,1000)
.add({
	targets: '.mv04',
	opacity: 1,
} ,1200);

var logoTimeline = anime.timeline({
	duration: 3000,
	easing: 'easeOutExpo'
});
logoTimeline
.add({
	targets: '.logo-en',
	opacity: 1,
} ,300)
.add({
	targets: '.logo-jp',
	opacity: 1,
} ,400)
.add({
	targets: '.entry-box-inner',
	opacity: 1,
} ,500);

});
