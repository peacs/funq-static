$(function(){
  var pagetop = $('.top-btn');
  pagetop.hide();
  $(window).scroll(function () {
     if ($(this).scrollTop() > 600) {
          pagetop.fadeIn();
     } else {
          pagetop.fadeOut();
     }
  });
  pagetop.click(function () {
     $('body, html').animate({ scrollTop: 0 }, 500);
     return false;
  });
});
